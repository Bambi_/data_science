# Exam - Algorithms & Data Structures:
# Bambach, Nico - 8315304
# 29.10.2020

# Description of Algorithm: for this algorithm you need to search for the smallest element which is in all lists - as
# the lists are ordered and have no duplicates you can just start from the first element of the first row list. Start
# checking if the first element of the first row(list) is existing in the second, in the third ... to the n. list if
# this is the case you can return that element and the algorithm is done. If the first element of the first row list
# is not in all row(lists) you take the next element and check if it is in the other lists repeat that until you find
# an element / number that is in all lists if you do not find any element that fulfills that conditions return -1


# In general you can stop checking for elements within one row list, if the element you are searching for is smaller
# than the current element of the row list - this may save you time (however i solved it with an "while element in
# row list" in my algorithm, which means that one could optimise it if you would either compare every element of a
# row list, until you find the correct element or until the element you are looking for is smaller than the current
# element you are comparing the row list to


# Assumptions for my implementation: The row lists in the input only contain integers and have all the same length


# Time complexity - description: time complexity is not linear. It is quadratic/ exponential (on average)because the
# operations that you need are increasing non linear, if you either increase the number of elements within the row or
# the columns you can also see that in the code, as we are having two loops within this function


# Implementing the Algorithm


mat = [[1, 2, 3, 4, 5], [2, 4, 5, 8, 10], [3, 5, 7, 9, 11], [1, 3, 5, 7, 9]]


def return_smallest_common_element(matrix):
    for e in matrix[0]:
        n = 1
        while n < len(matrix[0]) - 1 and e in matrix[n]:
            if n == len(matrix) - 1:
                return e
            n += 1
    return '-1'


print(return_smallest_common_element(mat))

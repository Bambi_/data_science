# 1. Assignment
# learn PEP-8 and PEP-20

# Determine if a given string is palindrome

def is_palindrome(string):
    a = string[::-1]
    if a.lower() == string.lower():
        return True
    return False


print(is_palindrome("anna"))


# this algorithm has time complexity n

# Explain the Code:
# starting with the first letter - comparing it to the last letter and looping through
# until the middle of the word is reached

# Estimation of big O:
# O(n)/2/2 -> because on average you need to check have of the first have of the word
# order of n ; or writing constant would also be correct

# create a faster algorithm for Palindrome
def is_palindrome2(word):
    i = 0
    j = len(word) - 1
    while i < j:
        if word[i] == word[j]:
            i = i + 1
            j = j - 1
        else:
            return False
    return True


print(is_palindrome2("aanaa"))


# 2 find all palindrome sections of any given string longer than one using the existing function is_palindrome2()


# Longest Substring without repeating characters / also working for numbers
def longest_substring_not_repeating(word):
    e = 0
    result = ''
    while e < len(word) - 1:
        n = e + 1
        print(result)
        while word[n] not in word[e:n] and n < len(word) - 1:
            n += 1
        if word[n] not in word[e:n]:
            n += 1
            if len(word[e:n]) < len(result):
                e += 1
            else:
                result = word[e:n]
                e += 1
        if len(word[e:n]) < len(result):
            e += 1
        else:
            result = word[e:n]
            e += 1
    return result


print(longest_substring_not_repeating('abcdbcdbefgh'))

# turn Numbers to English Numbers -> is not working!
dict_ = {1000000000: 'Billion', 1000000: 'Million', 1000: 'Thousand', 100: 'Hundred', 90: 'Ninty', 80: 'Eighty',
         70: 'Seventy',
         60: 'Sixty', 50: 'Fifty', 40: 'Fourty', 30: 'Thirty', 20: 'Twenty', 10: 'Ten', 9: 'Nine',
         8: 'Eight', 7: 'Seven', 6: 'Six', 5: 'Five', 4: 'Four', 3: 'Three', 2: 'Two', 1: 'One', 0: ''}


def numbers_to_english(input):
    result = []
    rest = input

    for number, english in dict_.items():
        if rest // number > 0:
            if number < 100:
                result.append(english)
                rest = rest % number
                if rest == 0:
                    return "".join(result)
                else:
                    result.append(' ')
                print(result)
            elif rest // number in dict_:
                result.append(dict_[rest // number])
                result.append(' ')
                result.append(english)
                result.append(' ')
                rest = rest % number
                print(result)
            else:
                print(rest % number)
                if rest // number > 99:
                    result.append(dict_[rest // number // 100])
                    result.append(' Hundret ')
                    result.append(dict_[rest // 100 % 10])
                    result.append(' ')
                    result.append(english)
                    result.append(' ')
                    rest = rest % number
                elif rest // number > 20:
                    result.append(dict_[(rest // number) - (rest // number % 10)])
                    result.append(' ')
                    result.append(dict_[rest // number % 10])
                    result.append(' ')
                    print(result)
                    result.append(english)
                    result.append(' ')
                    rest = rest % number
                else:
                    result.append(dict_[rest // number])
                    result.append(' ')
                    result.append(english)
                    result.append(' ')
                    rest = rest % number

    return "".join(result)


print(numbers_to_english(1681029))

# get rolling average of a list with a window - e.g. average of the first 2 elements , than 2&3,...
a = [1, 2, 3, 4, 5, 6, 7]


def get_rolling_average(a_list, window):
    result = []
    for i in range(0, len(a_list) - window + 1):
        sum = 0
        for j in range(0, window):
            sum = sum + a_list[i + j]
        result.append(sum / window)
    return result


print(get_rolling_average(a, 3))

# Print out all Fibonacci numbers smaller or equal to a given number
dict_ = {1: 1, 2: 1, 3: 1}
print(dict_)


def fibonacci(number):
    if number not in dict_:
        dict_[number] = fibonacci(number - 1) + fibonacci(number - 2)
    return dict_[number]


print('fibonacci =', fibonacci(10))
print(dict_)


def fibonacci_smaler_than_given(number):
    result = []
    e = 1
    while e < 1000:
        if e not in dict_:
            fibonacci(e)
            if dict_[e] > number:
                return result
            else:
                result.append[dict_[e]]
                e += 1
        else:
            if dict_[e] > number:
                return result
            else:
                result.append(dict_[e])
                e += 1
    return result


print(fibonacci_smaler_than_given(11))

# Determine if a sequence of numbers is increasing
list_ = [1, 2, 3, 4, 5, 3, 6, 7]


def is_increasing(sequence):
    # the first element (position 0) does not have to be checked as there is nothing to compare with
    for e in range(1, len(sequence)):
        if sequence[e - 1] < sequence[e]:
            continue
        else:
            return False
    return True


print(is_increasing(list_))


# Anagram - same from left and right
def is_anagram(string1, string2):
    if string1 == string2[::-1]:
        return True
    else:
        return False


print('Is this an anagram?', is_anagram('anagram', 'margana'))

# Calculate the frequency of items in a given sequence:
list_ = ['hi', 'I', 'am', 'Alexa', 'I']


def find_frequency_of_items(sequence):
    dict_ = {}
    for e in sequence:
        if e in dict_:
            dict_[e] += 1
        else:
            dict_[e] = 1
    return dict_


dict_ = find_frequency_of_items(list_)
print(dict_)


# Cryptography - Cypher
# take a clear text and key
# split everything into numbers
# add the key numbers to the clear numbers -> to get the new numbers

class VigenereCipher:
    pos_to_letter = {0: ' ', 1: 'A',  # dict is a dangerous name!
                     2: 'B',
                     3: 'C',
                     4: 'D',
                     5: 'E',
                     6: 'F',
                     7: 'G',
                     8: 'H',
                     9: 'I',
                     10: 'J',
                     11: 'K',
                     12: 'L',
                     13: 'M',
                     14: 'N',
                     15: 'O',
                     16: 'P',
                     17: 'Q',
                     18: 'R',
                     19: 'S',
                     20: 'T',
                     21: 'U',
                     22: 'V',
                     23: 'W',
                     24: 'X',
                     25: 'Y',
                     26: 'Z'}
    letter_to_pos = {v: k for k, v in pos_to_letter.items()}  # This is called a List Comprehension!

    def __init__(self, key):
        self.key = self.convert_to_numbers(key)

    def convert_to_numbers(self, text):
        numbers = []
        for c in text.upper():
            numbers.append(VigenereCipher.letter_to_pos[c])
        return numbers

    def convert_to_string(self, numbers):
        letters = []
        for i in numbers:
            letters.append(VigenereCipher.pos_to_letter[i])
        return "".join(letters)

    def encrypt(self, message):
        message_as_numbers = self.convert_to_numbers(message)
        i = 0
        encrypted = []
        while i < len(message_as_numbers):
            m = message_as_numbers[i]
            k = self.key[i % len(self.key)]
            encrypted.append((m + k) % 26)
            i += 1
        return self.convert_to_string(encrypted)

    def decrypt(self, message):
        message_as_numbers = self.convert_to_numbers(message)
        i = 0
        decrypted = []
        while i < len(message_as_numbers):
            m = message_as_numbers[i]
            k = self.key[i % (len(self.key))]
            decrypted.append((m - k) % 26)
            i += 1
        return self.convert_to_string(decrypted)


v = VigenereCipher("bambach")
c = v.encrypt("This Code is not working if we use z as an element")
m = v.decrypt(c)
print(c)
print(m)

# search numbers in a sequence, that add up to a number
list_ = [3, 4, 1, 7, 9, 17]


def add_up_to(sequence, number):
    solution = []
    sequence = drop_sort(sequence, number)
    n = 0
    # return None, if the highest numbers added are smaller than the desired number
    if (sequence[len(sequence) - 1] + sequence[len(sequence) - 2]) < number:
        return None
    while n < len(sequence) - 1:
        m = n + 1
        while m < (len(sequence)):
            if (sequence[n] + sequence[m]) == number:
                solution.append(sequence[n])
                solution.append(sequence[m])
                return solution
            if sequence[n] + sequence[m] > number:
                break
            else:
                m += 1
        n += 1
    return None


# for add_up_to
def drop_sort(list_, max_number):
    if len(list_) == 1:
        if list_[0] > max_number:
            return []
        else:
            return list_

    mid = len(list_) // 2
    left = list_[:mid]
    left = drop_sort(left, max_number)
    right = list_[mid:]
    right = drop_sort(right, max_number)

    return reduce(left, right)


# for add_up_to
def reduce(left, right):
    if len(left) == 0:
        return right
    if len(right) == 0:
        return left

    result = []
    l = 0
    r = 0
    while True:
        if left[l] < right[r]:
            result.append(left[l])
            l = l + 1
        else:
            result.append(right[r])
            r = r + 1

        if l == len(left):
            result.extend(right[r:])
            break
        if r == len(right):
            result.extend(left[l:])
            break
    return result


print(" = ", add_up_to(list_, 8))

# determine the longest sequence of increasing numbers in a list of numbers
list_ = [1, 2, 3, 2, 5, 6, 7, 8, 9, 10, 11, 12, 13]


def get_longest_sequence(sequence):
    n = 0
    check_next = []
    longest_sequence = []
    while n < len(sequence) - 1:
        if sequence[n] < sequence[n + 1]:
            n += 1
            check_next.append(sequence[n])
            if len(longest_sequence) < len(check_next):
                longest_sequence = check_next
        else:
            check_next = []
            n += 1
    return longest_sequence


print(get_longest_sequence(list_))


# creating roman numbers / convert to roman and convert to integer
class RomanNumbers:
    arabic_roman = [(1000, 'M'),
                    (900, 'CM'),
                    (500, 'D'),
                    (400, 'CD'),
                    (100, 'C'),
                    (90, 'XC'),
                    (50, 'L'),
                    (40, 'XL'),
                    (10, 'X'),
                    (9, 'IX'),
                    (5, 'V'),
                    (4, 'IV'),
                    (1, 'I')]

    roman_arabic = {'M': 1000, 'CM': 900, 'D': 500, 'CD': 400, 'C': 100, 'XC': 90, 'L': 50, 'XL': 40, 'X': 10, 'IX': 9,
                    'V': 5, 'IV': 4, 'I': 1}

    def convert_to_integer(roman):
        number = 0
        e = 0
        while e < len(roman) - 1:
            if RomanNumbers.roman_arabic.get(roman[e + 1]) > RomanNumbers.roman_arabic.get(roman[e]):
                number = number + RomanNumbers.roman_arabic.get(roman[e:e + 2])
                e += 2
            # this is not really needed for an working algorithm
            elif RomanNumbers.roman_arabic.get(roman[e + 1]) == RomanNumbers.roman_arabic.get(roman[e]):
                count = 2
                while RomanNumbers.roman_arabic.get(roman[e + count]) == RomanNumbers.roman_arabic.get(roman[e]):
                    count += 1
                number = number + count * RomanNumbers.roman_arabic.get(roman[e])
                e = e + count
            else:
                number = number + RomanNumbers.roman_arabic.get(roman[e])
                e += 1
        return number

    def convert_to_roman_1(number):
        roman = ''
        while number > 0:
            for a, r in RomanNumbers.arabic_roman:
                while number >= a:
                    roman += r
                    number -= a
        return roman

    def convert_to_roman_2(number):
        result = []
        for (arabic, roman) in RomanNumbers.arabic_roman:
            (factor, number) = divmod(number, arabic)
            result.append(roman * factor)
            if number == 0:
                break
        return "".join(result)


print(RomanNumbers.convert_to_roman_1(199))
input_roman = RomanNumbers.convert_to_roman_2(199)
print(input_roman)
print(RomanNumbers.convert_to_integer(input_roman))
print(RomanNumbers.convert_to_integer('XXXVIV'))

# Calculating Mean
list_ = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def get_mean(numbers, window):
    i = 0
    average = []
    while i < (len(numbers) - window):
        n = i
        sum = 0
        while n < i + window:
            sum = sum + numbers[n]
            n += 1
        average.append((sum / window))
        i += 1
    return average


print(get_mean(list_, 4))

# Calculating Median with Sliding window (e.g. for the first 3 than the 2-4 element, ...)
list_ = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def median_window(numbers, window):
    i = 0
    median = []
    while i < (len(numbers) - window):
        current_median = 0
        if window % 2 == 0:
            current_median = (numbers[i + (window // 2)] + numbers[i + (window // 2) + 1]) / 2
            median.append(current_median)
        else:
            current_median = numbers[(i + (window // 2))]
            median.append(current_median)
        i += 1
    return median


print(median_window(list_, 3))


# Password Checker is not working!
def check_password(string):
    final_result = 0
    final_result += check_if_to_short(string)
    final_result += check_if_to_long(string)
    final_result += check_upper(string)
    final_result += check_lower(string)
    final_result += check_digit(string)
    final_result += check_repeating(string)
    return final_result


def check_if_to_short(string):
    result = 0
    if len(string) < 6:
        result = 6 - len(string)
    return result


def check_if_to_long(string):
    result = 0
    if len(string) > 20:
        result = len(string) - 20
    return result


def check_upper(string):
    count = 0
    for e in (string):
        if e.isupper():
            count += 1
        return count
    return count


def check_lower(string):
    count = 0
    for e in (string):
        if e.islower():
            count += 1
        return count
    return count


def check_digit(string):
    count = 0
    for e in (string):
        if e.islower():
            count += 1
        return count
    return count


def check_repeating(string):
    result = 0
    e = 0
    while e < len(string) - 2:
        if string[e] == string[e + 1]:
            if string[e + 1] == string[e + 2]:
                result = 1
                return result
            e += 1
        e += 1
    return result


print("We need at maximum:", check_password("testtt"), "changes in the password")

# Maximum Profit from Trading Stocks - is not working
# Missing the difference between
list_ = [1, 3, 5, 4]


def maximum_profit(prices):
    total_profit = []

    e = 0
    while e < len(prices):
        buy = prices[e]
        s = e + 1
        while s < len(prices):
            profit = prices[s] - buy
            total_profit.append((e, s, profit))
            s += 1
        e += 1
    return total_profit


print(maximum_profit(list_))


# Check Word Abbreviation under given rules e.g. a number replaces a amount of letters!
def word_abbreviation(word, short):
    e = 0
    s = 0
    while s < len(short):
        digits = []
        # checking the digit, if the digit is not the last element of the short version
        if short[s].isdigit() and s < (len(short) - 1):
            while short[s].isdigit() and s < (len(short) - 1):
                digits.append(short[s])
                print(digits)
                s += 1
            number = int("".join(digits))
            print(number)
            e = e + number
            if word[e] == short[s]:
                continue
            else:
                return False
        # checking the digit if the 1. if statement is not True -> the digit is not the last digit
        elif short[s].isdigit():
            if len(word) - e == int(short[s]):
                return True
            else:
                return False
        elif short[s] == word[e]:
            e += 1
            s += 1
        else:
            return False

    return True


print(word_abbreviation("thisisatesta", "t10a"))

# Design Search Autocomplete System:

def merge_sort (list_):
    if len(list_)<= 1:
        return list_
    
    mid = len(list_)//2
    left = list_[:mid]
    left = merge_sort(left)
    right = list_[mid:]
    right = merge_sort(right)
    
    return reduce(left, right)
    
    

def reduce (left, right):
    result = []
    l = 0
    r = 0
    while True:  
        if left[l] < right [r]:
            result.append(left[l])
            l = l + 1
        else:
            result.append(right[r])
            r = r + 1
    
        if l == len(left):
            result.extend(right[r:])
            break
        if r == len(right):
            result.extend(left[l:])
            break
    return result



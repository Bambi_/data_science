import random
import time
import merge_sort

anzahl = [125,250, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000]
best = list(range(0,len(anzahl))) 

y=0
for x in anzahl:
    li = list(range(0,x))
    
    start = time.time()
    merge_sort.merge_sort(li)
    end = time.time()
    
    best[y] = end-start
    y = y + 1


best_des = list(range(0,len(anzahl))) 

y=0
for x in anzahl:
    li = list(range(0,x))
    li = li[::-1]
    start = time.time()
    merge_sort.merge_sort(li)
    end = time.time()
    
    best_des [y] = end-start
    y = y + 1


average = list(range(0,len(anzahl))) 

y=0
for x in anzahl:
    li = list(range(0,x))
    random.shuffle(li)
    start = time.time()
    merge_sort.merge_sort(li)
    end = time.time()
    
    average[y] = end-start
    y = y + 1


import numpy as np
import pandas as pd

anzahl = pd.Series(anzahl)
best = pd.Series(best)
average = pd.Series(average)
best_des = pd.Series(best_des)

df = pd.concat([anzahl, best, average, best_des], axis=1)
df = pd.DataFrame ({'elements':anzahl, 'best':best, 'average':average, 'best_des':best_des})

print(df)

import matplotlib.pyplot as plt
plt.xlabel('Number of Elements')
plt.ylabel('Time in Seconds')

plt.plot(anzahl, best, label='best Case')
plt.plot (anzahl, best_des, label='best_des Case')
plt.plot (anzahl, average, label='average Case')


plt.legend(loc='upper left', title='Legend', frameon=False)

plt.show()


import os
import numpy as np
import pandas as pd
import sklearn as sk
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix


# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)
# load data and preprocess it
os.chdir ('C:/Users/NicoBambach/OneDrive - proresult/Master/Semester 1/Data Analytics/Data/')
data_ori = pd.read_csv('YoungBank.csv')

print(data_ori.head())
print(data_ori.isnull().sum())


print(data_ori.shape)
# types
print(data_ori.dtypes)
# feature names
print(list(data_ori))
# head
print(data_ori.head(6))
# descriptions, change precision to 2 places
print(data_ori.describe())

print(data_ori['Education'].value_counts())

from sklearn.preprocessing import LabelBinarizer
bi_Edu = LabelBinarizer()
bi_dummys = bi_Edu.fit_transform(data_ori['Education'])
Edu_dummys = pd.DataFrame(bi_dummys,columns=['Edu_gra', 'Edu_prof', 'Edu_under']) 

print(Edu_dummys)
print(Edu_dummys['Edu_under'].value_counts())
del Edu_dummys['Edu_under']

data_final = pd.concat([data_ori, Edu_dummys], axis=1)
data_final = data_final.drop('Education', axis=1)

print(data_final['Personal_Loan'].value_counts())

# Downsampling
data_final_majority = data_final[data_final.Personal_Loan == 'no']
data_final_minority = data_final[data_final.Personal_Loan == 'yes']

from sklearn.utils import resample
data_final_majority_downsampled = resample(data_final_majority,
n_samples=len(data_final_minority),replace=False, random_state=0) 
data_final_downsampled = pd.concat([data_final_majority_downsampled, data_final_minority])


################
#   decide if you want to use downsampled data              #
#   therfore use data_final_downsampled for creating X and y#
###############






X_ori = data_final.drop('Personal_Loan', axis = 1)
Y = data_final['Personal_Loan']

# normalize
X = (X_ori-X_ori.min())/(X_ori.max()-X_ori.min())
print(X.head(6))





from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.50)
print(Y_train.value_counts())
print(Y_test.value_counts())





#create report dataframe
###############
# offen:
###############
# add the specific hyper-parameters, that lead to the highest result, to the report df
report = pd.DataFrame(columns=['Model','Acc. Train' ,'Acc. Test', 'F1-Score Test', 'ROC'])


################
#     KNN      #
################

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
#find optimal k for the KNN Model with maximum e neighbors
e = 20
train_accuracies = np.zeros((3,e-1), float)
for k in range(1, e):
    knnmodel = KNeighborsClassifier(n_neighbors=k)
    accuracies = cross_val_score(knnmodel, X_train, Y_train, scoring='accuracy', cv=10)
    knnmodel.fit(X_train, Y_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()
print(train_accuracies)

# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('Number of Neighbors')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()


opt_k = np.argmax(train_accuracies[1,:])+1
print('Optimal k =', opt_k)

knnmodel = KNeighborsClassifier(n_neighbors=opt_k)
knnmodel.fit(X_train, Y_train)
Y_train_pred = knnmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
acctr = accuracy_score(Y_train, Y_train_pred)
Y_test_pred = knnmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
accte = accuracy_score(Y_test, Y_test_pred)


# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(knnmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d') 
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)



#calculate ROC and AUC and plot the curve
Y_probs = knnmodel.predict_proba(X_test)
print(Y_probs[0:6,:])
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
print(Y_test_probs[0:6])
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])
print (fpr, tpr, threshold)
from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)
print(roc_auc)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of k-NN')
plt.show()

report.loc[len(report)] = ['k-NN', acctr, accte, f1te, roc_auc]

###############
# Naive Bayes #
###############

from sklearn.naive_bayes import GaussianNB
nbmodel = GaussianNB()
nbmodel.fit(X_train, Y_train)

Y_train_pred = nbmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)

Y_test_pred = nbmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(nbmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = nbmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Naive Bayes')
plt.show()


report.loc[len(report)] = ['Naive Bayes', acctr, accte, f1te, roc_auc]

######################
#   Decision Trees   #
######################

from sklearn.tree import DecisionTreeClassifier

#Build model where e is the max_depth testet
e = 20
train_accuracies = np.zeros((3,e-1), float)
for k in range(1, e):
    etmodel = DecisionTreeClassifier(criterion='entropy', random_state=0, max_depth=k, class_weight='balanced')
    accuracies = cross_val_score(etmodel, X_train, Y_train, scoring='accuracy', cv=10)
    etmodel.fit(X_train, Y_train)
    Y_train_pred = etmodel.predict(X_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()


# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('Max_depth')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()

# finding the maximum to test it
opt_k = np.argmax(train_accuracies[1,:])+1
print('Optimal k =', opt_k)

etmodel = DecisionTreeClassifier(criterion='entropy', random_state=0, max_depth=opt_k, class_weight='balanced')
etmodel.fit(X_train, Y_train)
Y_train_pred = etmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = etmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(etmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)



#calculate ROC and AUC and plot the curve
Y_probs = etmodel.predict_proba(X_test)
print(Y_probs[0:6,:])
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
print(Y_test_probs[0:6])
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])
print (fpr, tpr, threshold)
from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)
print(roc_auc)


#show feature importance
list(zip(X, etmodel.feature_importances_))
index = np.arange(len(etmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, etmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Decision Tree - Feature Importance')
plt.show()


report.loc[len(report)] = ['Decision Tree', acctr, accte, f1te, roc_auc]



# #=============================================================================
# #show tree using graphviz
# import graphviz 
# dot_data = sk.tree.export_graphviz(etmodel, out_file=None,
#                           feature_names=list(X),  
#                           filled=True, rounded=True,  
#                           special_characters=True) 
# graph = graphviz.Source(dot_data) 
# graph.format = 'png'
# graph.render("Churn_entropy") 
# #=============================================================================

#################
#     Gini      #
#################
#Build Gini model where e is the max_depth testet
e = 20
train_accuracies = np.zeros((3,e-1), float)
for k in range(1, e):
    gtmodel = DecisionTreeClassifier(random_state=0, max_depth=k, class_weight='balanced')
    accuracies = cross_val_score(gtmodel, X_train, Y_train, scoring='accuracy', cv=10)
    gtmodel.fit(X_train, Y_train)
    Y_train_pred = gtmodel.predict(X_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()


# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('Max_depth')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Gini - Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()

# finding the maximum to test it
opt_k = np.argmax(train_accuracies[1,:])+1
print('Optimal k for Gini =', opt_k)



gtmodel = DecisionTreeClassifier(random_state=0, max_depth=opt_k, class_weight='balanced')
gtmodel.fit(X_train, Y_train)
Y_train_pred = gtmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = gtmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(gtmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)

#calculate ROC and AUC and plot the curve
Y_probs = gtmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Gini')
plt.show()



#show feature importance
list(zip(X, gtmodel.feature_importances_))
index = np.arange(len(gtmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, gtmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Gini - Feature Importance')
plt.show()


report.loc[len(report)] = ['Gini', acctr, accte, f1te, roc_auc]


#################
# Random Forest #
#################
from sklearn.ensemble import RandomForestClassifier

#################
#varying max_depth
#################
e=20
train_accuracies = np.zeros((3,e-1), float)
for k in range(1, e):
    rfmodel = RandomForestClassifier(random_state=0, max_depth=k, class_weight='balanced')
    accuracies = cross_val_score(rfmodel, X_train, Y_train, scoring='accuracy', cv=10)
    rfmodel.fit(X_train, Y_train)
    Y_train_pred = rfmodel.predict(X_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()

# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('Max_depth')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Random Forest - Varying Depth  - Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()

# finding the maximum to test it
opt_k = np.argmax(train_accuracies[1,:])+1
print('Optimal k =', opt_k)

rfmodel = RandomForestClassifier(random_state=0, max_depth=opt_k, class_weight='balanced')
rfmodel.fit(X_train, Y_train)
Y_train_pred = rfmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Varying Depth - Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = rfmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Varying Depth - Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(rfmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()


#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = rfmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Random Forest - Varing Depht')
plt.show()



#show feature importance
list(zip(X, rfmodel.feature_importances_))
index = np.arange(len(rfmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, rfmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Random Forest - Varing Depht - Feature Importance')
plt.show()


report.loc[len(report)] = ['Random-Forest - Varying Depht', acctr, accte, f1te, roc_auc]

#################
# Random Forest #
#################
#####################
#varying n_estimators
#####################
e = 20
train_accuracies = np.zeros((3,e-1), float)
ntrees = (np.arange(20)+1)*10
for k in range(1, e):
    rfmodel = RandomForestClassifier(random_state=0, n_estimators=ntrees[k], class_weight='balanced')
    accuracies = cross_val_score(rfmodel, X_train, Y_train, scoring='accuracy', cv=10)
    rfmodel.fit(X_train, Y_train)
    Y_train_pred = rfmodel.predict(X_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()

# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('n_estimators')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Random Forest - Varying n_estimators  - Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()

# finding the maximum to test it
opt_k = np.argmax(train_accuracies[1,:]) +1
print('Optimal k =', opt_k)

rfmodel = RandomForestClassifier(random_state=0, max_depth=opt_k, class_weight='balanced')
rfmodel.fit(X_train, Y_train)
Y_train_pred = rfmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Varying Depth - Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = rfmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Varying Depth - Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(rfmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = rfmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Random Forest - Varying n_estimators')
plt.show()



#show feature importance
list(zip(X, rfmodel.feature_importances_))
index = np.arange(len(rfmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, rfmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Random Forest - Varing n_estimators - Feature Importance')
plt.show()


report.loc[len(report)] = ['Random-Forest - Varying n_estimators', acctr, accte, f1te, roc_auc]


#################
# Random Forest #
#################
###################################
#varying max_depth and n_estimators
###################################

d_start = 4         # start of depht
d_end = 20          # end of depht
d_count = 5         # number s between start and end
n = 5              # number of differenets estimators
steps_tree = 10     # increasing amound of Trees eacht step

mdepth = np.linspace(d_start, d_end, d_count)
ntrees = (np.arange(n)+1)*steps_tree
train_accuracies = np.zeros((4,d_count*n), float)
row = 0
for k in range(0, d_count):
    for l in range(0, n):
        rfmodel = RandomForestClassifier(random_state=0, max_depth=mdepth[k], n_estimators=ntrees[l], class_weight='balanced')
        accuracies = cross_val_score(rfmodel, X_train, Y_train, scoring='accuracy', cv=10)
        rfmodel.fit(X_train, Y_train)
        Y_train_pred = rfmodel.predict(X_train)
        acctr = accuracy_score(Y_train, Y_train_pred)
        train_accuracies[2,row] = accuracies.mean()
        train_accuracies[3,row] = accuracies.std()
        train_accuracies[0,row] = mdepth[k]
        train_accuracies[1,row] = ntrees[l]
        row = row + 1


from tabulate import tabulate
headers = ["Max_Depth", "n_Estimators", "Mean Accurancie", "Standard Deviation"]
table = tabulate(train_accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
print("\n",table)

print(train_accuracies[2].max())
maxi = np.array(np.where(train_accuracies==train_accuracies[2].max()))
print(maxi[0,:], maxi[1,:])
print(train_accuracies[:,maxi[1,:]])
table = tabulate(train_accuracies[:,maxi[1,:]].transpose(), headers, tablefmt="plain", floatfmt=".3f")
print('The best Acc is: ',"\n",table)


opt_k = np.argmax(train_accuracies[2,:]) 
print(train_accuracies.transpose()[opt_k])
print(train_accuracies.transpose()[opt_k,0])
print(train_accuracies.transpose()[opt_k,1])
rfmodel = RandomForestClassifier(random_state=0, max_depth=train_accuracies.transpose()[opt_k,0],
                                 n_estimators= int(train_accuracies.transpose()[opt_k,1]), class_weight='balanced')
rfmodel.fit(X_train, Y_train)
Y_train_pred = rfmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = rfmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(rfmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = rfmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Random Forest - Varing depth and n_estimators')
plt.show()



#show feature importance
list(zip(X, rfmodel.feature_importances_))
index = np.arange(len(rfmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, rfmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Random Forest - Varing depth and n_estimators - Feature Importance')
plt.show()


report.loc[len(report)] = ['Random-Forest - Varying depth and n_estimators', acctr, accte, f1te, roc_auc]


# View a list of the features and their importance scores
list(zip(X_train, rfmodel.feature_importances_))

##########
# Work done up to here!
# don't forget to add to the functions for getting a balanced class
# , class_weight='balanced'


################################
# Gradient Boosting Classifier #
################################
from sklearn.ensemble import GradientBoostingClassifier

#varying max_depth and learning_rate
n = 20      # Input of number of learning rates tested
d_count = 20     # max dept of gradient Boosting Classifier
lr = np.linspace(0, 0.4, n)
lr[0] = 0.01
row = 0
train_accuracies = np.zeros((4,d_count*n), float)
for k in range(0, d_count):
    for l in range(0, n):
        gbmodel = GradientBoostingClassifier(random_state=0, max_depth=k+1, learning_rate=lr[l])
        accuracies = cross_val_score(gbmodel, X_train, Y_train, scoring='accuracy', cv=10)
        gbmodel.fit(X_train, Y_train)
        Y_train_pred = gbmodel.predict(X_train)
        acctr = accuracy_score(Y_train, Y_train_pred)
        train_accuracies[2,row] = accuracies.mean()
        train_accuracies[3,row] = accuracies.std()
        train_accuracies[0,row] = k + 1
        train_accuracies[1,row] = lr[l]
        row = row + 1

from tabulate import tabulate
headers = ["Max_depth", "Learning_rate", "Mean Accurancie Training", "Standard Deviation"]
table = tabulate(train_accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
print("\n",table)

maxi = np.array(np.where(train_accuracies==train_accuracies[3].max()))
print(maxi[1,:])
print(train_accuracies[:,maxi[1,:]])
table = tabulate(train_accuracies[:,maxi[1,:]].transpose(), headers, tablefmt="plain", floatfmt=".3f")
print("\n",table)


from sklearn.ensemble import GradientBoostingClassifier
gbmodel = GradientBoostingClassifier(random_state=0, max_depth=5, learning_rate=0.34)
gbmodel.fit(X_train, Y_train)
Y_train_pred = gbmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = gbmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)
report.loc[len(report)] = ['Gradient Boosting', acctr, accte]


opt_k = np.argmax(train_accuracies[2,:]) 
print(train_accuracies.transpose()[opt_k])
print(train_accuracies.transpose()[opt_k,0])
print(train_accuracies.transpose()[opt_k,1])
gbmodel = GradientBoostingClassifier(random_state=0, max_depth=train_accuracies.transpose()[opt_k,0], 
                                     learning_rate=train_accuracies.transpose()[opt_k,1])
gbmodel.fit(X_train, Y_train)
Y_train_pred = gbmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = gbmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(gbmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = gbmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Gradient Boosting Classifier')
plt.show()



#show feature importance
list(zip(X, gbmodel.feature_importances_))
index = np.arange(len(gbmodel.feature_importances_))
bar_width = 1.0
plt.bar(index, gbmodel.feature_importances_, bar_width)
plt.xticks(index,  list(X), rotation=90) # labels get centered
plt.title('Gradient Boosting Classifier - Feature Importance')
plt.show()


report.loc[len(report)] = ['Gradient Boosting Classifier', acctr, accte, f1te, roc_auc]

#########################
# Discriminant Analysis #
#########################

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
dismodel = LinearDiscriminantAnalysis()
dismodel.fit(X_train, Y_train)
Y_train_pred = dismodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = dismodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(dismodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = dismodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Linear Discriminant Analysis')
plt.show()



report.loc[len(report)] = ['Linear Discriminant Analysis', acctr, accte, f1te, roc_auc]

##########
#Quadratic Disciminant Analysis
#########
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
qdismodel = QuadraticDiscriminantAnalysis()
qdismodel.fit(X_train, Y_train)
Y_train_pred = qdismodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = qdismodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)


# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(qdismodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = qdismodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Quadratic Discriminant Analysis')
plt.show()

report.loc[len(report)] = ['Quadratic Discriminant Analysis', acctr, accte, f1te, roc_auc]

#######################
# Logistic Regression #
#######################

from sklearn.linear_model import LogisticRegression
lrmodel = LogisticRegression()
lrmodel.fit(X_train, Y_train)

Y_train_pred = lrmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)

Y_test_pred = lrmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)

# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(lrmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = lrmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Logistic Regression')
plt.show()

report.loc[len(report)] = ['Logistic Regression', acctr, accte, f1te, roc_auc]



##################
# Neural Network #
##################

from sklearn.neural_network import MLPClassifier
e = 30
train_accuracies = np.zeros((3,e-1), float)
for k in range(1, e):
    nnetmodel = MLPClassifier(solver='lbfgs', hidden_layer_sizes=(k,), random_state=0, activation='relu')
    accuracies = cross_val_score(nnetmodel, X_train, Y_train, scoring='accuracy', cv=10)
    nnetmodel.fit(X_train, Y_train)
    Y_train_pred = nnetmodel.predict(X_train)
    train_accuracies[0,k-1] = k
    train_accuracies[1,k-1] = accuracies.mean()
    train_accuracies[2,k-1] = accuracies.std()

# creating a Plot
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('Hidden_Layers')
ax1.set_ylabel('Mean Accuracie', color=color)
ax1.plot(train_accuracies[0,:], train_accuracies[1,:], color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Standard Deviation', color=color)  # we already handled the x-label with ax1
ax2.plot(train_accuracies[0,:], train_accuracies[2,:], color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.title('Neural Network  - Comparison of Accuracies and Standard Deviation')
plt.xticks(range(1, e))
plt.show()


# finding the maximum to test it
opt_k = np.argmax(train_accuracies[1,:]) +1
print('Optimal k =', opt_k)
nnetmodel = MLPClassifier(solver='lbfgs', hidden_layer_sizes=(opt_k,), random_state=0)
nnetmodel.fit(X_train, Y_train)
Y_train_pred = nnetmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr)
Y_test_pred = nnetmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n", cmte)
accte = accuracy_score(Y_test, Y_test_pred)
print("Accurray Test:", accte)


# Visualize Confusion Matrix
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
plot_confusion_matrix(nnetmodel, X_test, Y_test, labels= ['no', 'yes'], 
                      cmap=plt.cm.Blues, values_format='d')
plt.show()

#calculate f1 score
from sklearn.preprocessing import LabelEncoder
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)


#calculate ROC and AUC and plot the curve
Y_probs = nnetmodel.predict_proba(X_test)
Y_test_probs = np.array(np.where(Y_test=='yes', 1, 0))
from sklearn.metrics import roc_curve
fpr, tpr, threshold = roc_curve(Y_test_probs, Y_probs[:, 1])

from sklearn.metrics import auc
roc_auc = auc(fpr, tpr)

import matplotlib.pyplot as plt
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.title('ROC Curve of Neural Network')
plt.show()

report.loc[len(report)] = ['Neural Network', acctr, accte, f1te, roc_auc]
print(report)

report.to_csv('report.csv', index=False)

##################################
#       Break for now --- comparing downsampled with not downsampled #

# report_both = pd.concat ([report_final, report], axis = 1)

########
# Everything that is comming comment out for not - it is not used!
########

# ##########################
# # Support Vector Machine #
# ###########################

# #linear kernel
# from sklearn.svm import SVC
# LinSVCmodel = SVC(kernel='linear', C=10, random_state=0, class_weight='balanced')
# accuracies = cross_val_score(LinSVCmodel, X_train, Y_train, scoring='accuracy', cv=10)
# LinSVCmodel.fit(X_train, Y_train)
# Y_train_pred = LinSVCmodel.predict(X_train)
# cmtr = confusion_matrix(Y_train, Y_train_pred)
# print("Confusion Matrix Training:\n", cmtr)
# acctr = accuracy_score(Y_train, Y_train_pred)
# print("Accurray Training:", acctr)
# Y_test_pred = LinSVCmodel.predict(X_test)
# cmte = confusion_matrix(Y_test, Y_test_pred)
# print("Confusion Matrix Testing:\n", cmte)
# accte = accuracy_score(Y_test, Y_test_pred)
# print("Accurray Test:", accte)


# accuracies = np.zeros((3,21), float)
# costs = np.linspace(0, 40, 21)
# costs[0] = 0.5
# for k in range(0, 21):
#     LinSVCmodel = SVC(kernel='linear', C=costs[k], random_state=0)
#     LinSVCmodel.fit(X_train, Y_train)
#     Y_train_pred = LinSVCmodel.predict(X_train)
#     acctr = accuracy_score(Y_train, Y_train_pred)
#     accuracies[1,k] = acctr
#     Y_test_pred = LinSVCmodel.predict(X_test)
#     accte = accuracy_score(Y_test, Y_test_pred)
#     accuracies[2,k] = accte
#     accuracies[0,k] = costs[k]
# plt.plot(costs, accuracies[1,:])
# plt.plot(costs, accuracies[2,:])
# plt.xlim(1,20)
# plt.xticks(costs, rotation=90)
# plt.xlabel('Cost')
# plt.ylabel('Accuracy')
# plt.title('Linear SVM')
# plt.show()

# from tabulate import tabulate
# headers = ["Cost", "acctr", "accte"]
# table = tabulate(accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# #radial kernel
# from sklearn.svm import SVC
# accuracies = np.zeros((3,21), float)
# costs = np.linspace(0, 40, 21)
# costs[0] = 0.5
# for k in range(0, 21):
#     RbfSVCmodel = SVC(kernel='rbf', C=costs[k], gamma=0.2, random_state=0)
#     RbfSVCmodel.fit(X_train, Y_train)
#     Y_train_pred = RbfSVCmodel.predict(X_train)
#     acctr = accuracy_score(Y_train, Y_train_pred)
#     accuracies[1,k] = acctr
#     Y_test_pred = RbfSVCmodel.predict(X_test)
#     accte = accuracy_score(Y_test, Y_test_pred)
#     accuracies[2,k] = accte
#     accuracies[0,k] = costs[k]
# plt.plot(costs, accuracies[1,:])
# plt.plot(costs, accuracies[2,:])
# plt.xlim(1,20)
# plt.xticks(costs, rotation=90)
# plt.xlabel('Cost')
# plt.ylabel('Accuracy')
# plt.title('Radial SVM')
# plt.show()

# from tabulate import tabulate
# headers = ["Cost", "acctr", "accte"]
# table = tabulate(accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# accuracies = np.zeros((3,21), float)
# gammas = np.linspace(0, 4.0, 21)
# gammas[0] = 0.1
# for k in range(0, 21):
#     RbfSVCmodel = SVC(kernel='rbf', C=1, gamma=gammas[k], random_state=0)
#     RbfSVCmodel.fit(X_train, Y_train)
#     Y_train_pred = RbfSVCmodel.predict(X_train)
#     acctr = accuracy_score(Y_train, Y_train_pred)
#     accuracies[1,k] = acctr
#     Y_test_pred = RbfSVCmodel.predict(X_test)
#     accte = accuracy_score(Y_test, Y_test_pred)
#     accuracies[2,k] = accte
#     accuracies[0,k] = gammas[k]
# plt.plot(gammas, accuracies[1,:])
# plt.plot(gammas, accuracies[2,:])
# plt.xticks(gammas, rotation=90)
# plt.xlabel('Gamma')
# plt.ylabel('Accuracy')
# plt.title('Radial SVM')
# plt.show()

# from tabulate import tabulate
# headers = ["Gamma", "acctr", "accte"]
# table = tabulate(accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# n = 21
# accuracies = np.zeros((4,n*n), float)
# costs = np.linspace(0, 20, n)
# costs[0] = 0.5
# gammas = np.linspace(0, 4.0, n)
# gammas[0] = 0.1
# row = 0
# for k in range(0, n):
#     for l in range(0, n):
#         RbfSVCmodel = SVC(kernel='rbf', C=costs[k], gamma=gammas[l], random_state=0)
#         RbfSVCmodel.fit(X_train, Y_train)
#         Y_train_pred = RbfSVCmodel.predict(X_train)
#         acctr = accuracy_score(Y_train, Y_train_pred)
#         accuracies[2,row] = acctr
#         Y_test_pred = RbfSVCmodel.predict(X_test)
#         accte = accuracy_score(Y_test, Y_test_pred)
#         accuracies[3,row] = accte
#         accuracies[0,row] = costs[k]
#         accuracies[1,row] = gammas[l]
#         row = row + 1

# from tabulate import tabulate
# headers = ["Cost", "Gamma", "acctr", "accte"]
# table = tabulate(accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# maxi = np.array(np.where(accuracies==accuracies[3].max()))
# print(maxi[1,:])
# print(accuracies[:,maxi[1,:]])
# table = tabulate(accuracies[:,maxi[1,:]].transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# from mpl_toolkits.mplot3d import Axes3D
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# x = accuracies[0,:]
# y = accuracies[1,:]
# z = accuracies[3,:]
# ax.plot_trisurf(x, y, z, linewidth=0.2, antialiased=True)
# ax.set_xlabel('Cost')
# ax.set_ylabel('Gamma')
# ax.set_zlabel('accte')
# plt.show()

# RbfSVCmodel = SVC(kernel='rbf', C=14, gamma=0.4 , random_state=0)
# RbfSVCmodel.fit(X_train, Y_train)
# Y_train_pred = RbfSVCmodel.predict(X_train)
# cmtr = confusion_matrix(Y_train, Y_train_pred)
# print("Confusion Matrix Training:\n", cmtr)
# acctr = accuracy_score(Y_train, Y_train_pred)
# print("Accurray Training:", acctr)
# Y_test_pred = RbfSVCmodel.predict(X_test)
# cmte = confusion_matrix(Y_test, Y_test_pred)
# print("Confusion Matrix Testing:\n", cmte)
# accte = accuracy_score(Y_test, Y_test_pred)
# print("Accurray Test:", accte)
# report.loc[len(report)] = ['SVM (Radial)', acctr, accte]


# #polynomial kernel
# n = 21
# accuracies = np.zeros((4,n*n), float)
# costs = np.linspace(0, 20, n)
# costs[0] = 0.5
# degrees = np.linspace(0, 10.0, n)
# degrees[0] = 0.1
# row = 0
# for k in range(0, n):
#     for l in range(0, n):
#         PolySVCmodel = SVC(kernel='poly', C=costs[k], degree=degrees[l], random_state=0)
#         PolySVCmodel.fit(X_train, Y_train)
#         Y_train_pred = PolySVCmodel.predict(X_train)
#         acctr = accuracy_score(Y_train, Y_train_pred)
#         accuracies[2,row] = acctr
#         Y_test_pred = PolySVCmodel.predict(X_test)
#         accte = accuracy_score(Y_test, Y_test_pred)
#         accuracies[3,row] = accte
#         accuracies[0,row] = costs[k]
#         accuracies[1,row] = gammas[l]
#         row = row + 1

# from tabulate import tabulate
# headers = ["Cost", "Degree", "acctr", "accte"]
# table = tabulate(accuracies.transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)

# maxi = np.array(np.where(accuracies==accuracies[3:].max()))
# print(maxi[1,:])
# print(accuracies[:,maxi[1,:]])
# table = tabulate(accuracies[:,maxi[1,:]].transpose(), headers, tablefmt="plain", floatfmt=".3f")
# print("\n",table)


# ################
# # Final Report #
# ################

# print(report)
